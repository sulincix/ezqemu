build:
	: please run make install
install:
	mkdir -p $(DESTDIR)/usr/bin
	mkdir -p $(DESTDIR)/usr/share/applications/
	install main.py $(DESTDIR)/usr/bin/ezqemu
	install qemu-run $(DESTDIR)/usr/bin/qemu-run
	install ezqemu.desktop  $(DESTDIR)/usr/share/applications/
