#!/usr/bin/env python3
import gi
import os
import subprocess

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk

home = os.environ["HOME"]

from ctypes import *
libc = CDLL("libc.so.6")
libc.setenv.argtypes = [c_char_p, c_char_p, c_int]

def setenv(var, val):
    libc.setenv(var.encode("utf-8"),val.encode("utf-8"),1)

class EasyQemuApp(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Easy Qemu")
        self.set_border_width(10)
        self.set_default_size(400, 300)

        # Main box
        self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(self.box)


        # Main options
        create_button = Gtk.Button.new_with_label("Create Virtual Machine")
        create_button.connect("clicked", self.on_create_clicked)
        self.box.add(create_button)

        run_button = Gtk.Button.new_with_label("Run Virtual Machine")
        run_button.connect("clicked", self.on_run_clicked)
        self.box.add(run_button)

        edit_button = Gtk.Button.new_with_label("Edit Virtual Machine")
        edit_button.connect("clicked", self.on_edit_clicked)
        self.box.add(edit_button)

        exit_button = Gtk.Button.new_with_label("Exit")
        exit_button.connect("clicked", self.on_exit_clicked)
        self.box.add(exit_button)

        # Show all widgets
        self.show_all()


    def change_config(self, name,var, val):
        new_ctx = ""
        with open(f"{home}/.local/ezqemu/{name}/config","r") as f:
           for line in f.read().split("\n"):
               if line.startswith(var+"="):
                   new_ctx += var+"="+val+"\n"
               else:
                   new_ctx += line+"\n"
        with open(f"{home}/.local/ezqemu/{name}/config","w") as f:
            print(new_ctx)
            f.write(new_ctx)
            f.flush()

    def on_exit_clicked(self, widget):
        Gtk.main_quit()

    def on_create_clicked(self, widget):
        self.hide()
        dialog = Gtk.Dialog(title="Create Virtual Machine", parent=self, flags=Gtk.DialogFlags.MODAL)
        dialog.add_button("Create", Gtk.ResponseType.OK)
        dialog.add_button("Cancel", Gtk.ResponseType.CANCEL)

        name_entry = Gtk.Entry()
        name_entry.set_placeholder_text("Enter name of new profile")

        ram_entry = Gtk.Entry()
        ram_entry.set_placeholder_text("Enter size of virtual RAM (example: 1024M, 2G)")

        disk_entry = Gtk.Entry()
        disk_entry.set_placeholder_text("Enter new virtual disk size (example: 100M, 3G, 1T)")

        dialog_box = dialog.get_content_area()
        dialog_box.add(name_entry)
        dialog_box.add(ram_entry)
        dialog_box.add(disk_entry)

        dialog.show_all()
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            name = name_entry.get_text()
            ramsize = ram_entry.get_text()
            disk = disk_entry.get_text()

            os.makedirs(f"{home}/.local/ezqemu/{name}", exist_ok=True)
            subprocess.run(["qemu-img", "create", "-f", "qcow2",
                            f"{home}/.local/ezqemu/{name}/image.qcow2", disk])

            with open(f"{home}/.local/ezqemu/{name}/config", "w") as f:
                f.write("VIRTIO=true\n")
                f.write("UEFI=true\n")
                f.write(f"RAM={ramsize}\n")
                f.write("HEADLESS=false\n")
                f.write("noINFO=true\n")
                port = os.getpid() % 10000 + 1024
                f.write(f"PORT={port}\n")

        dialog.destroy()
        self.show()

    def on_run_clicked(self, widget):
        self.hide()
        dialog = Gtk.Dialog(title="Run Virtual Machine", parent=self, flags=Gtk.DialogFlags.MODAL)
        dialog.add_button("Cancel", Gtk.ResponseType.CANCEL)

        # List available profiles
        profiles = [profile for profile in os.listdir(f"{home}/.local/ezqemu") if not profile.startswith(".")]
        if not profiles:
            dialog_label = Gtk.Label(label="No profiles found")
            dialog.get_content_area().add(dialog_label)
            dialog.show_all()
            response = dialog.run()
            dialog.destroy()
            return

        profile_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        for profile in profiles:
            profile_button = Gtk.Button.new_with_label(profile)
            profile_button.connect("clicked", self.on_profile_selected, profile)
            profile_box.pack_start(profile_button, True, True, 0)

        dialog.get_content_area().add(profile_box)
        dialog.show_all()
        response = dialog.run()
        dialog.destroy()
        self.show()

    def on_profile_selected(self, widget, profile):
        print(profile)
        if os.path.isfile(f"{home}/.local/ezqemu/{profile}/config"):
            with open(f"{home}/.local/ezqemu/{profile}/config","r") as f:
                for line in f.read().split("\n"):
                    var = line.split("=")[0]
                    val = line[len(var)+1:]
                    setenv(var,val)
        os.system("env | sort")
        iso = f"{home}/.local/ezqemu/{profile}/iso"
        cmd = ["qemu-run", f"{home}/.local/ezqemu/{profile}/image.qcow2"]
        if os.path.exists(iso):
            cmd += [iso]
        subprocess.Popen(cmd)
        Gtk.main_quit()




    def on_edit_clicked(self, widget):
        self.hide()
        dialog = Gtk.Dialog(title="Edit Virtual Machine", parent=self, flags=Gtk.DialogFlags.MODAL)
        dialog.add_button("Save", Gtk.ResponseType.OK)
        dialog.add_button("Cancel", Gtk.ResponseType.CANCEL)

        # List available profiles
        profiles = [profile for profile in os.listdir(f"{home}/.local/ezqemu") if not profile.startswith(".")]
        if not profiles:
            dialog_label = Gtk.Label(label="No profiles found")
            dialog.get_content_area().add(dialog_label)
            dialog.show_all()
            response = dialog.run()
            dialog.destroy()
            return

        profile_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        for profile in profiles:
            profile_button = Gtk.Button.new_with_label(profile)
            profile_button.connect("clicked", self.edit_profile, profile, dialog)
            profile_box.pack_start(profile_button, True, True, 0)

        dialog_box = dialog.get_content_area()
        dialog_box.add(profile_box)

        dialog.show_all()
        response = dialog.run()

        dialog.destroy()
        self.show()

    def edit_profile(self, widget, profile, fdialog):
        self.hide()
        fdialog.destroy()
        dialog = Gtk.Dialog(title="Edit Menu", parent=self, flags=Gtk.DialogFlags.MODAL)
        dialog.add_button("Cancel", Gtk.ResponseType.CANCEL)

        edit_menu_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        rename_button = Gtk.Button.new_with_label("Rename Profile")
        rename_button.connect("clicked", self.on_rename_clicked, profile)
        edit_menu_box.pack_start(rename_button, True, True, 0)

        iso_button = Gtk.Button.new_with_label("Select Iso")
        iso_button.connect("clicked", self.on_iso_clicked, profile)
        edit_menu_box.pack_start(iso_button, True, True, 0)

        delete_button = Gtk.Button.new_with_label("Delete Profile")
        delete_button.connect("clicked", self.on_delete_clicked, profile)
        edit_menu_box.pack_start(delete_button, True, True, 0)

        disk_button = Gtk.Button.new_with_label("Resize Disk Image")
        disk_button.connect("clicked", self.on_disk_clicked, profile)
        edit_menu_box.pack_start(disk_button, True, True, 0)

        ram_button = Gtk.Button.new_with_label("Resize RAM Size")
        ram_button.connect("clicked", self.on_ram_clicked, profile)
        edit_menu_box.pack_start(ram_button, True, True, 0)

        dialog_box = dialog.get_content_area()
        dialog_box.add(edit_menu_box)

        dialog.show_all()
        response = dialog.run()

        dialog.destroy()
        self.show()



    def on_iso_clicked(self, widget, profile=None):
        self.hide()
        dialog = Gtk.FileChooserDialog(
            title="Select ISO Image",
            parent=self,
            action=Gtk.FileChooserAction.OPEN,
            buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK),
        )
        dialog.set_default_size(800, 400)
        dialog.set_select_multiple(False)
        filter_iso = Gtk.FileFilter()
        filter_iso.set_name("ISO Images")
        filter_iso.add_mime_type("application/x-iso9660-image")
        dialog.add_filter(filter_iso)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            selected_iso = dialog.get_filename()
            print(f"Selected ISO: {selected_iso}")
            if os.path.exists(f"{home}/.local/ezqemu/{profile}/iso"):
                os.unlink(f"{home}/.local/ezqemu/{profile}/iso")
            os.symlink(selected_iso, f"{home}/.local/ezqemu/{profile}/iso")
        dialog.destroy()
        self.show()

    def on_rename_clicked(self, widget, profile):
        dialog = Gtk.Dialog(title="Rename Profile", parent=self, flags=Gtk.DialogFlags.MODAL)
        dialog.add_button("Rename", Gtk.ResponseType.OK)
        dialog.add_button("Cancel", Gtk.ResponseType.CANCEL)

        name_entry = Gtk.Entry()
        name_entry.set_placeholder_text("Enter new name for the profile")

        dialog_box = dialog.get_content_area()
        dialog_box.add(name_entry)

        dialog.show_all()
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            new_name = name_entry.get_text()
            os.rename(f"{home}/.local/ezqemu/{profile}", f"{home}/.local/ezqemu/{new_name}")
            print(f"Profile renamed to: {new_name}")

        dialog.destroy()

    def on_delete_clicked(self, widget, profile):
        dialog = Gtk.Dialog(title="Delete Profile", parent=self, flags=Gtk.DialogFlags.MODAL)
        dialog.add_button("Delete", Gtk.ResponseType.OK)
        dialog.add_button("Cancel", Gtk.ResponseType.CANCEL)

        dialog_label = Gtk.Label(label="Are you sure you want to delete this profile?")
        dialog.get_content_area().add(dialog_label)

        dialog.show_all()
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            shutil.rmtree(f"{home}/.local/ezqemu/{profile}")
            print("Profile deleted")

        dialog.destroy()

    def on_disk_clicked(self, widget, profile):
        dialog = Gtk.Dialog(title="Resize Disk Image", parent=self, flags=Gtk.DialogFlags.MODAL)
        dialog.add_button("Resize", Gtk.ResponseType.OK)
        dialog.add_button("Cancel", Gtk.ResponseType.CANCEL)

        size_entry = Gtk.Entry()
        size_entry.set_placeholder_text("Enter new size for the disk (example: 100M, 3G, 1T)")

        dialog_box = dialog.get_content_area()
        dialog_box.add(size_entry)

        dialog.show_all()
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            new_size = size_entry.get_text()
            subprocess.Popen(["qemu-img", "resize",
                f"{home}/.local/ezqemu/{profile}/image.qcow2", new_size],
                stdout=subprocess.DEVNULL,
                stdin=subprocess.DEVNULL,
                stderr=subprocess.STDOUT)
            print(f"Disk resized to: {new_size}")

        dialog.destroy()

    def on_ram_clicked(self, widget, profile):
        self.hide()
        dialog = Gtk.Dialog(title="Resize RAM", parent=self, flags=Gtk.DialogFlags.MODAL)
        dialog.add_button("Resize", Gtk.ResponseType.OK)
        dialog.add_button("Cancel", Gtk.ResponseType.CANCEL)

        size_entry = Gtk.Entry()
        size_entry.set_placeholder_text("Enter new size for the RAM (example: 1024M, 2G)")

        dialog_box = dialog.get_content_area()
        dialog_box.add(size_entry)

        dialog.show_all()
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            new_size = size_entry.get_text()
            self.change_config(profile, "RAM", new_size)
            print(f"RAM resized to: {new_size}")

        dialog.destroy()

def main():
    app = EasyQemuApp()
    Gtk.main()

if __name__ == "__main__":
    main()
